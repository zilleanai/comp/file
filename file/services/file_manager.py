from flask_unchained.bundles.sqlalchemy import ModelManager

from ..models import File


class FileManager(ModelManager):
    class Meta:
        model = File