"""
    file
    ~~~~

    Component for file in general.

    :copyright: Copyright © 2018 chriamue
    :license: Not open source, see LICENSE for details
"""

__version__ = '0.1.0'


from flask_unchained import Bundle


class FileBundle(Bundle):

    name = 'file_bundle'

    @classmethod
    def before_init_app(cls, app):
        pass

    @classmethod
    def after_init_app(cls, app):
        pass
