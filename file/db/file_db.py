import os
from backend.config import Config as AppConfig

import sqlite3


class FileDB():
    def __init__(self, project):
        BASE_DIR = os.path.join(
            AppConfig.DATA_FOLDER, project)
        self.project = project
        self.conn = sqlite3.connect(os.path.join(BASE_DIR, 'database.db'))
        self.conn.row_factory = sqlite3.Row
        self.createTables()

    def createTables(self):
        c = self.conn.cursor()
        c.execute(
            '''CREATE TABLE IF NOT EXISTS `file` (filename text, path text, PRIMARY KEY (filename, path));''')
        c.execute(
            '''CREATE TABLE IF NOT EXISTS `file_tags` (filename text, path text, tag text, PRIMARY KEY (filename, path, tag));''')
        self.conn.commit()

    def addFile(self, filename, path, tags):
        c = self.conn.cursor()
        c.execute('INSERT OR IGNORE INTO `file` VALUES (?,?)', (filename, path))
        self.setTags(filename, path, tags)
        self.conn.commit()

    def getTags(self, filename, path):
        c = self.conn.cursor()
        rows = c.execute(
            '''SELECT tag FROM `file_tags` WHERE filename=? AND path=?''', (filename, path)).fetchall()
        tags = [ix[0] for ix in rows]
        return tags

    def setTags(self, filename, path, tags):
        c = self.conn.cursor()
        c.execute(
            '''DELETE FROM `file_tags` WHERE filename=? AND path=?''', (filename, path))
        for t in tags:
            c.execute('INSERT INTO `file_tags` VALUES (?,?,?)',
                      (filename, path, t))
        self.conn.commit()

    def getFilePathsByTag(self, tag):
        c = self.conn.cursor()
        rows = c.execute(
            '''SELECT path, filename FROM `file_tags` WHERE tag=?''', (tag,)).fetchall()
        files = [(ix[0], ix[1]) for ix in rows]
        return files