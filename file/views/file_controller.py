# source: https://stackoverflow.com/questions/23718236/python-flask-browsing-through-directory-with-files
# https://stackoverflow.com/questions/27337013/how-to-send-zip-files-in-the-python-flask-framework

import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
from werkzeug.utils import secure_filename
from bundles.project.services import ProjectManager
from bundles.tag.services import TagManager
from ..services import FileManager
from ..db import FileDB


# source: https://stackoverflow.com/questions/10480440/zip-folder-with-subfolder-in-python
def zipfolder(memory_file, target_dir):            
    zipobj = zipfile.ZipFile(memory_file, 'w', zipfile.ZIP_DEFLATED)
    rootlen = len(target_dir)
    for base, dirs, files in os.walk(target_dir):
        for file in files:
            fn = os.path.join(base, file)
            zipobj.write(fn, fn[rootlen:])


class FileController(Controller):

    file_manager: FileManager = injectable
    project_manager: ProjectManager = injectable
    tag_manager: TagManager = injectable

    @route('/list/<string:project>', defaults={'req_path': ''})
    @route('/list/<string:project>/<path:req_path>')
    def listing(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            # Show directory contents
            files = os.listdir(abs_path)
            resp = jsonify(project=project,
                           path=req_path, files=files)
        return resp

    @route('/files/<string:project>')
    def files_root(self, project):
        return self.files(project, '')

    @route('/files/<string:project>/<path:req_path>')
    def files(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            # Show directory contents
            files = [f for f in os.listdir(abs_path)
                     if os.path.isfile(os.path.join(abs_path, f))]
            resp = jsonify(project=project,
                           path=req_path, files=files)
        return resp

    @route('/dirs/<string:project>')
    def dirs_root(self, project):
        return self.dirs(project, '')

    @route('/dirs/<string:project>/<path:req_path>')
    def dirs(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)
        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            # Show directory contents
            dirs = [d for d in os.listdir(abs_path)
                    if os.path.isdir(os.path.join(abs_path, d))]
            resp = jsonify(project=project,
                           path=req_path, dirs=dirs)
        return resp

    @route('/download/<string:project>')
    def download_root(self, project):
        return self.download(project, '/')

    @route('/download/<string:project>/<path:req_path>')
    def download(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, '' if req_path=='/' else req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        print(BASE_DIR, req_path, abs_path)
        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            return send_file(abs_path)
        # Show directory contents
        files = os.listdir(abs_path)
        memory_file = BytesIO()

        zipfolder(memory_file, abs_path)
        memory_file.seek(0)
        zipfilename = project if req_path=='/' else os.path.basename(abs_path) + ".zip"
        return send_file(memory_file, attachment_filename=zipfilename, as_attachment=True)

    @route('/delete/<string:project>/<path:req_path>', methods=['POST'])
    def delete(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            os.remove(abs_path)
            return jsonify(success=True, project=project, file=req_path)
        return abort(404)

    @route('/upload/<string:project>/<string:tags>', methods=['POST'])
    def upload_root(self, project, tags):
        return self.upload(project, '', tags)

    @route('/upload/<string:project>/<path:req_path>/<string:tags>', methods=['POST'])
    def upload(self, project, req_path, tags):
        tags = tags.split(",")
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        BASE_DIR = os.path.join(BASE_DIR, req_path)
        os.makedirs(BASE_DIR, exist_ok=True)
        if 'filepond' not in request.files:
            return abort(404)
        file = request.files['filepond']
        if file.filename == '':
            return abort(404)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(BASE_DIR, filename))
            FileDB(project).addFile(filename, req_path, tags)
        return jsonify(success=True)

    @route('/newdir/<string:project>', methods=['POST'])
    def newdir_root(self, project):
        return self.newdir(project, '')

    @route('/newdir/<string:project>/<path:req_path>', methods=['POST'])
    def newdir(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER
        name = secure_filename(request.json['name'])
        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path, name)
        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            os.makedirs(abs_path, exist_ok=True)
            return jsonify(success=True)
        return jsonify(error='already exists')

    @route('/tags/<string:project>/<path:req_path>', methods=['GET'])
    def filetags(self, project, req_path):
        (path, filename) = os.path.split(req_path)
        tags = FileDB(project).getTags(filename, path)
        return jsonify(project=project, path=path, filename=filename, tags=tags)

    @route('/tags/<string:project>/<path:req_path>', methods=['PUT'])
    def put_filetags(self, project, req_path):
        tags = request.json['tags']
        (path, filename) = os.path.split(req_path)
        FileDB(project).setTags(filename, path, tags)
        return jsonify(success=True)
