import requests
from io import BytesIO
from PIL import Image


class file():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def list(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'file/list/' + \
            str(self.project['name']) + path
        return requests.get(url).json()['files']

    def files(self, path: str = '/'):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'file/files/' + \
            str(self.project['name']) + path
        return requests.get(url).json()['files']

    def dirs(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'file/dirs/' + \
            str(self.project['name']) + path
        return requests.get(url).json()['dirs']

    def download(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + \
            'file/download/' + str(self.project['name']) + file_name
        response = requests.get(url)
        data = BytesIO(response.content)
        return data

    def tags(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + \
            'file/tags/' + str(self.project['name']) + file_name
        return requests.get(url).json()['tags']
