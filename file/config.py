from flask_unchained import BundleConfig


class Config(BundleConfig):
	WTF_CSRF_ENABLED = False
