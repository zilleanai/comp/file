from flask_unchained import (controller, resource, func, include, prefix,
                             get, delete, post, patch, put, rule)

from .views import FileController


routes = lambda: [
    prefix('/api/v1', [
        prefix('/file', [
            controller(FileController),
        ]),
    ]),
]
