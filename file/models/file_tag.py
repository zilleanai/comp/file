from flask_unchained.bundles.sqlalchemy import db


class FileTag(db.Model):
    """Join table between File and Tag"""
    # __tablename__ = 'file_tag'

    file_id = db.foreign_key('File', primary_key=True)
    file = db.relationship('File', back_populates='file_tags')

    tag_id = db.foreign_key('Tag', primary_key=True)
    tag = db.relationship('Tag')

    __repr_props__ = ('file_id', 'tag_id')

    def __init__(self, file=None, tag=None, **kwargs):
        super().__init__(**kwargs)
        if file:
            self.file = file
        if tag:
            self.tag = tag