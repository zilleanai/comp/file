from flask_unchained.bundles.sqlalchemy import db
from .file_tag import FileTag

class File(db.Model):
    name = db.Column(db.String(127))
    path = db.Column(db.String(255), nullable=True)

    project_id = db.foreign_key('Project')
    
    file_tags = db.relationship('FileTag', back_populates='file',
                             cascade='all, delete-orphan')
    tags = db.association_proxy('file_tags', 'tag',
                             creator=lambda tag: FileTag(tag=tag))

    __repr_props__ = ('id', 'name')
