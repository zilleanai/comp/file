import { get, post, put } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function file(uri) {
    return v1(`/file${uri}`)
}

export default class File {
    static listFiles(payload) {
        return get(file(`/files/${storage.getProject()}${payload.path.replace('//', '/')}`))
    }
    static listDirs(payload) {
        return get(file(`/dirs/${storage.getProject()}${payload.path.replace('//', '/')}`))
    }

    static listFileTags({ project, path, filename }) {
        return get(file(`/tags/${project}${path.replace('//', '/')}/${filename}`))
    }

    static tagFile({ project, path, filename, tags }) {
        return put(file(`/tags/${project}${path.replace('//', '/')}/${filename}`), { 'tags': tags })
    }

    /**
    * @param {string} id
    */
    static download(id) {
        return get(file(`/download/${id}`))
    }

    static deletePath({ path, file: file_ }) {
        return post(file(`/delete/${storage.getProject()}${path}/${file_}`), {})
    }

    static newDir({ path, name }) {
        return post(file(`/newdir/${storage.getProject()}${path}`), { 'name': name })
    }
}
