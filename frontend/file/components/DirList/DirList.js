import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { listDirs } from 'comps/file/actions'
import { selectDirsList } from 'comps/file/reducers/dirs'
import { storage } from 'comps/project'
import { Dir } from 'comps/file/components'
import './dir-list.scss'

class DirList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    const { path, listDirs } = this.props
    listDirs.maybeTrigger({
      project: storage.getProject(), path
    })
  }

  componentWillReceiveProps(nextProps) {
    const { path } = nextProps
    this.props.listDirs.maybeTrigger({
      project: storage.getProject(), path
    }
    )
  }

  createButton = () => {
    const { error, pristine, submitting } = this.props
    return (<div className="row">
    </div>)
  }

  decorator = (dir, path, i) => {
    return (
      <li key={i}>
        <Dir path={path} dir={dir} />
      </li>
    )
  }

  render() {
    const { dirs, path, decorator } = this.props
    const decoratorFn = decorator || this.decorator
    if (!dirs || dirs.length === 0) {
      return (<div className="dir-list"><p>No dirs.</p>{this.createButton()}</div>)
    }
    return (
      <div className="dir-list">
        <ul >
          <div>
            {dirs.map((dir, i) => {
              return (decoratorFn(dir, path, i))
            }
            )}
          </div>
        </ul>
        {this.createButton()}
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/file/reducers/dirs'))
const withSaga = injectSagas(require('comps/file/sagas/dirs'))

const withConnect = connect(
  (state, props) => {
    const { path } = props
    const dirs = selectDirsList(state, storage.getProject(), path)
    return {
      dirs
    }
  },
  (dispatch) => bindRoutineCreators({ listDirs }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(DirList)
