import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { v1 } from 'api'
import { storage } from 'comps/project'
import { deletePath } from 'comps/file/actions'
import './file.scss'

class File extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      deleted: false
    }
  }

  render() {
    const { file, path, pristine, submitting } = this.props
    const { deleted } = this.state
    if (!file || !path || deleted) {
      return null
    }
    return (
      <div className='file'>
        {file}
        <div className='actions'>
          <button type="submit"
            onClick={(e) => {
              this.props.deletePath.trigger({
                path: (path == '/' ? '' : path),
                file
              })
              this.setState({ deleted: true })
            }}
            disabled={pristine || submitting}
          >
            {submitting ? 'Deleting...' : 'Delete'}
          </button>
          <a href={v1(`/file/download/${storage.getProject()}${path == '/' ? '' : path}/${file}`)}><button>Download</button></a>
        </div>
      </div>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    return {
    }
  },
  (dispatch) => bindRoutineCreators({ deletePath }, dispatch),
)

const withSaga = injectSagas(require('comps/file/sagas/deletePath'))

export default compose(
  withSaga,
  withConnect,
)(File)
