import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { reduxForm, change } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import './file-upload.scss'
import { TextField } from 'components/Form'
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import { storage } from 'comps/project'
import { newDir } from 'comps/file/actions'
const FORM_NAME = 'newDir'

class FileUpload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      files: [],
      showNewDir: false
    };
  }

  handleInit() {
  }

  render() {
    const { url, path, tags, handleSubmit, pristine, submitting } = this.props
    const { showNewDir } = this.state
    return (
      <div className="FileUpload">

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          maxFiles={100}
          server={`${url}/${storage.getProject()}${path == '/' ? '' : path}/${tags}`}
          oninit={() => this.handleInit()}
          onupdatefiles={(fileItems) => {
            // Set current file objects to this.state
            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}
          {this.state.files.map(file => (
            <File key={file} src={file} origin="local" />
          ))}

        </FilePond>
        <div className='newDir'>
          {
            showNewDir ? (
              <form onSubmit={handleSubmit(newDir)}>
                <TextField name='name' />
                <div className="row">
                  <button type="submit"
                    disabled={pristine || submitting}
                  >
                    {submitting ? 'Creating...' : 'Create'}
                  </button>
                </div>
              </form>
            ) : (
                <button onClick={
                  (e) => {
                    this.setState({ showNewDir: true })
                    this.props.change('path', path);
                  }
                }>New Folder</button>
              )

          }
        </div>

      </div>
    );
  }
}


const withConnect = connect(
  (state, props) => {
    const { path } = props
    const initialValues = { path }
    return {
      initialValues,
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch) => {
    dispatch(reset(FORM_NAME))
  }
})

const withSaga = injectSagas(require('comps/file/sagas/newDir'))

export default compose(
  withSaga,
  withConnect,
  withForm,
)(FileUpload)
