import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { listFiles } from 'comps/file/actions'
import { selectFilesList } from 'comps/file/reducers/files'
import { storage } from 'comps/project'
import { File } from 'comps/file/components'
import './file-list.scss'

class FileList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    const { path, listFiles } = this.props
    listFiles.maybeTrigger({
      project: storage.getProject(), path
    })
  }

  componentWillReceiveProps(nextProps) {
    const { path } = nextProps
    this.props.listFiles.maybeTrigger({
      project: storage.getProject(), path
    }
    )
  }

  createButton = () => {
    const { error, pristine, submitting } = this.props
    return (<div className="row">
    </div>)
  }

  decorator = (file, path, i) => {
    return (
      <li key={i}>
        <File path={path} file={file} />
      </li>
    )
  }

  render() {
    const { files, path, decorator } = this.props
    const decoratorFn = decorator || this.decorator
    if (!files || files.length === 0) {
      return (<div className="file-list"><p>No files.</p>{this.createButton()}</div>)
    }
    return (
      <div className="file-list" >
        <ul>
          <div>
            {files.map((file, i) => {
              return(decoratorFn(file, path, i))
            }
            )}
          </div>
        </ul>
        {this.createButton()}
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/file/reducers/files'))
const withSaga = injectSagas(require('comps/file/sagas/files'))

const withConnect = connect(
  (state, props) => {
    const { path } = props
    const files = selectFilesList(state, storage.getProject(), path)
    return {
      files
    }
  },
  (dispatch) => bindRoutineCreators({ listFiles }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FileList)
