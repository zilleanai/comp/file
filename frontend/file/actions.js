import { createRoutine } from 'actions'
export const listFiles = createRoutine('file/LIST_FILES')
export const listDirs = createRoutine('file/LIST_DIRS')
export const listFileTags = createRoutine('file/LIST_FILE_TAGS')
export const download = createRoutine('file/DOWNLOAD_FILE')
export const deletePath = createRoutine('file/DELETE_PATH')
export const tagFile = createRoutine('file/TAG_FILE')
export const newDir = createRoutine('file/NEW_DIR')