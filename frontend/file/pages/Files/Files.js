import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { v1 } from 'api'
import { PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { FileUpload, FileList, DirList } from 'comps/file/components'
import { TagSelect } from 'comps/tag/components'
import { storage } from 'comps/project'

class Files extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tags: [],
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.files-page',
          intro: 'Files can be uploaded, folders can be created on this page.',
        },
        {
          element: '.tag-select',
          intro: 'Select some tags from this list, which will be associated to uploaded files.',
        },
        {
          element: '.FileUpload',
          intro: 'Files can be uploaded here. Just drop files here.',
        },
        {
          element: '.newDir',
          intro: 'Create a new directory in this folder.',
        },
        {
          element: '.dir-list',
          intro: 'A list of subfolders.',
        },
        {
          element: '.file-list',
          intro: 'A list of files in current folder.',
        },
      ],
    }
  }

  handleTags = ({ value: tags }) => {
    this.setState({ tags });
  }

  render() {
    const { subpath } = this.props
    const { tags, stepsEnabled, steps, initialStep } = this.state
    return (
      <PageContent className='files-page'>
      <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Files</title>
        </Helmet>
        <h1>Files</h1>
        <TagSelect onChange={this.handleTags} />
        <FileUpload url={v1(`/file/upload`)} path={subpath} tags={tags} />
        <DirList path={subpath}/>
        <FileList path={subpath}/>
        <button><a href={v1(`/file/download/${storage.getProject()}${subpath == '/' ? '' : subpath}`)}>Download</a></button>
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const subpath = decodeURIComponent(props.match.params.subpath)
    return {
      subpath
    }
  },
  (dispatch) => bindRoutineCreators({ }, dispatch),
)

export default compose(
  withConnect
)(Files)