import { call, put, takeLatest } from 'redux-saga/effects'

import { deletePath } from 'comps/file/actions'
import { createRoutineSaga } from 'sagas'
import FileApi from 'comps/file/api'


export const KEY = 'deletePath'

export const deletePathSaga = createRoutineSaga(
    deletePath,
    function* successGenerator(payload) {
        payload = { path: payload.path, file: payload.file }
        const response = yield call(FileApi.deletePath, payload)
        yield put(deletePath.success(response))
    }
)

export default () => [
    takeLatest(deletePath.TRIGGER, deletePathSaga),
]
