import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listDirs } from 'comps/file/actions'
import FileApi from 'comps/file/api'
import { selectDirs } from 'comps/file/reducers/dirs'


export const KEY = 'dirs'

export const maybeListDirsSaga = function* (payload) {
  const { byPath, isLoading } = yield select(selectDirs)
  const path = payload.platform + payload.path
  const isLoaded = !!byPath[path]
  if (!(isLoaded || isLoading)) {
    yield put(listDirs.trigger(payload))
  }
}

export const listDirsSaga = createRoutineSaga(
  listDirs,
  function* successGenerator({ payload: payload }) {
    const dirs = yield call(FileApi.listDirs, payload)
    yield put(listDirs.success({
      dirs,
    }))
  },
)

export default () => [
  takeEvery(listDirs.MAYBE_TRIGGER, maybeListDirsSaga),
  takeLatest(listDirs.TRIGGER, listDirsSaga),
]
