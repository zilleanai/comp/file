import { call, put, takeLatest } from 'redux-saga/effects'

import { tagFile } from 'comps/file/actions'
import { createRoutineSaga } from 'sagas'
import FileApi from 'comps/file/api'


export const KEY = 'tagFile'

export const tagFileSaga = createRoutineSaga(
  tagFile,
  function *successGenerator(payload) {
    payload = { project: payload.project, path: payload.path, 'filename': payload.filename, 'tags': payload.tags }
    const response = yield call(FileApi.tagFile, payload)
    yield put(tagFile.success(response))}
)

export default () => [
  takeLatest(tagFile.TRIGGER, tagFileSaga),
]
