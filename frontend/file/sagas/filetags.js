import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listFileTags } from 'comps/file/actions'
import FileApi from 'comps/file/api'
import { selectFileTags } from 'comps/file/reducers/filetags'


export const KEY = 'file_tags'

export const maybeListFileTagsSaga = function* ({payload}) {
  const {project, path, filename} = payload
  const { byPath, isLoading } = yield select(selectFileTags)
  const p = project + path + filename
  const isLoaded = !!byPath[p]
  if (!(isLoaded || isLoading)) {
    yield put(listFileTags.trigger({project, path, filename}))
  }
}

export const listFileTagsSaga = createRoutineSaga(
  listFileTags,
  function* successGenerator({project, path, filename}) {
    const tags = yield call(FileApi.listFileTags, {project, path, filename})
    yield put(listFileTags.success({
      tags,
    }))
  },
)

export default () => [
  takeEvery(listFileTags.MAYBE_TRIGGER, maybeListFileTagsSaga),
  takeLatest(listFileTags.TRIGGER, listFileTagsSaga),
]
