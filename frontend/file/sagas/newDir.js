import { call, put, takeLatest } from 'redux-saga/effects'

import { newDir } from '../actions'
import { createRoutineFormSaga } from 'sagas'
import FileApi from '../api'


export const KEY = 'new_dir'

export const newDirSaga = createRoutineFormSaga(
  newDir,
  function* successGenerator(payload) {
    const response = yield call(FileApi.newDir, payload)
    yield put(newDir.success(response))
  }
)

export default () => [
  takeLatest(newDir.TRIGGER, newDirSaga)
]
