import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listFiles } from 'comps/file/actions'
import FileApi from 'comps/file/api'
import { selectFiles } from 'comps/file/reducers/files'


export const KEY = 'files'

export const maybeListFilesSaga = function* (payload) {
  const { byPath, isLoading } = yield select(selectFiles)
  const path = payload.project + payload.path
  const isLoaded = !!byPath[path]
  if (!(isLoaded || isLoading)) {
    yield put(listFiles.trigger(payload))
  }
}

export const listFilesSaga = createRoutineSaga(
  listFiles,
  function* successGenerator({ payload: payload }) {
    const files = yield call(FileApi.listFiles, payload)
    yield put(listFiles.success({
      files,
    }))
  },
)

export default () => [
  takeEvery(listFiles.MAYBE_TRIGGER, maybeListFilesSaga),
  takeLatest(listFiles.TRIGGER, listFilesSaga),
]
