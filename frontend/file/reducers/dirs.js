import { listDirs } from 'comps/file/actions'


export const KEY = 'dirs'

const initialState = {
  isLoading: false,
  isLoaded: false,
  paths: [],
  byPath: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { dirs } = payload || {}
  const { paths, byPath } = state

  switch (type) {
    case listDirs.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listDirs.SUCCESS:
    const path = dirs.project+(dirs.path==''?'/':'/'+dirs.path)
    if (!paths.includes(path)) {
      paths.push(path)
    }
    byPath[path] = dirs.dirs
      return {
        ...state,
        dirs: dirs.dirs,
        paths,
        byPath,
        isLoaded: true,
      }

    case listDirs.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listDirs.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectDirs = (state) => state[KEY]
export const selectDirsList = (state, project, path) =>{
  const p = project + path
  return selectDirs(state).byPath[p]
}
