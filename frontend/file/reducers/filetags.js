import { listFileTags } from 'comps/file/actions'


export const KEY = 'file_tags'

const initialState = {
  isLoading: false,
  isLoaded: false,
  paths: [],
  byPath: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { tags } = payload || {}
  const { paths, byPath } = state

  switch (type) {
    case listFileTags.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listFileTags.SUCCESS:
      let path = tags.project + '/' + tags.path + '/' + tags.filename
      path = path.replace('//', '/')
      if (!paths.includes(path)) {
        paths.push(path)
      }
      byPath[path] = tags.tags
      return {
        ...state,
        paths,
        byPath,
        isLoaded: true,
      }

    case listFileTags.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listFileTags.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectFileTags = (state) => state[KEY]
export const selectFileTagsList = (state, project, path, filename) => {
  let p = project + '/' + path + '/' + filename
  p = p.replace('//', '/')
  p = p.replace('//', '/')
  return selectFileTags(state).byPath[p]
}
