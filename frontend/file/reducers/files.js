import { listFiles } from 'comps/file/actions'


export const KEY = 'files'

const initialState = {
  isLoading: false,
  isLoaded: false,
  paths: [],
  byPath: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { files } = payload || {}
  const { paths, byPath } = state

  switch (type) {
    case listFiles.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listFiles.SUCCESS:
    const path = files.project+(files.path==''?'/':'/'+files.path)
    if (!paths.includes(path)) {
      paths.push(path)
    }
    byPath[path] = files.files
      return {
        ...state,
        files: files.files,
        paths,
        byPath,
        isLoaded: true,
      }

    case listFiles.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listFiles.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectFiles = (state) => state[KEY]
export const selectFilesList = (state, project, path) =>{
  const p = project + path
  return selectFiles(state).byPath[p]
}
