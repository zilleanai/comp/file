# file

Component for file in general.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/file
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.file',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.file.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Files
} from 'comps/file/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Files: 'Files',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Files,
    path: '/files/:subpath',
    component: Files,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Files} params={{subpath:'/'}}/>
    ...
</div>
```
